#include"Vehicle.h"
#include <map>
#include <iostream>
int main()
{
	std::map<Vehicle, std::string, Vehicle::Comparator>masina;

	Vehicle Ford("Ford", 1999);
	Vehicle Kia("Kia", 2019);
	Vehicle Dacia("Dacia", 2005);
	Vehicle Honda("Honda", 2003);
	Vehicle Renault("Renault", 2007);

	masina.emplace(Ford, "Ford");
	masina.emplace(Kia, "Kia");
	masina.emplace(Dacia, "Dacia");
	masina.emplace(Honda, "Honda");
	masina.emplace(Renault, "Renault");


	for (const auto & element : masina)
	{
		std::cout << element.second << '\n';
}
	system("pause");
}